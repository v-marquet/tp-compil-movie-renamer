#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//Global variables
char* file_name_buffer="[kickass.to](2000.laws).pirates.des.caraibes.1.la.malediction.du.black.pearl.1980.french.{2900189880234564}.dvdrip.divx.avi";

char* EXT=NULL;
char* VIDEO=NULL;
char* LANGUAGE=NULL;
char* DATE=NULL;
char* TITLE=NULL;

//fonctions

void getEXT(){
	int file_name_buffer_length = strlen(file_name_buffer);
	int i=file_name_buffer_length-1,j;
	char* TEMP=NULL;
	EXT = malloc( sizeof(char)*(file_name_buffer_length-i+1) );

	printf("\n\nDebug : Obtenir le format (EXT)\n");	
	
	while(file_name_buffer[i]!='.') i--;

	for(j=i; j<file_name_buffer_length; j++) EXT[j-i]=file_name_buffer[j];

	TEMP = malloc( sizeof(char)*(i) );
	for(j=0; j<i;j++) TEMP[j]=file_name_buffer[j];
	
	TEMP[j++] = '\0';

	file_name_buffer = TEMP;
}

void getVIDEO(){
	
	int i, j, n = 0, k =0, st =0;
	int *isquality = malloc(sizeof(int)*14), *length = malloc(sizeof(int)*14);
	int file_name_buffer_length = strlen(file_name_buffer);
	char* TEMP = malloc(sizeof(char)*file_name_buffer_length);
	
	printf("\n\nDebug : Obtenir la qualité (VIDEO)\n");
	
	for(i=0; i < 14; i++) isquality[i]=0;

	char* movie_quality[]={ "DVDRIP", "BRRIP", "BLURAY", "BLU-RAY", "480P", "576P", "720P", "1080P", "XVID", "DIVX", "X264", "MPEG2", "MPEG4", "AVC" };

	char *p = file_name_buffer;
		while (*p != '\0')
		{
			*p = toupper(*p);
			p++;
		}

	for(i = 0; i < 14; i++) {
		char *str = strstr(file_name_buffer, movie_quality[i]);
		file_name_buffer_length = strlen(file_name_buffer);
		if(str != NULL) {
			st = 1;
			int str_length = strlen(str);
			n++;
			isquality[i]=1;
			length[i] = strlen(movie_quality[i]);
			printf("l[%d] = %d\n",i,length[i]);

			for(j=0; j<file_name_buffer_length - str_length;j++)
				TEMP[j] = file_name_buffer[j];
			
			printf("temp = %s\n", TEMP);
			
			for(j=file_name_buffer_length - str_length; j<file_name_buffer_length; j++)
				TEMP[j]=file_name_buffer[j+length[i]];
			printf("temp = %s\n", TEMP);
		
		
			file_name_buffer = TEMP;
			}
	}

  if(st) {
	VIDEO = malloc(sizeof(char)*(7*n+n-1));
	
	for(i = 0; i < 14; i++) {
		if(isquality[i]==1) 
			for(j=k; j<k+length[i]; j++) 
				VIDEO[j] = movie_quality[i][j-k];
	VIDEO[j] = ' ';
	k = j+1;
	}
   }
	
}

void getLANGUAGE() {

	int i, j, n = 0, k =0, st = 0;
	int *islanguage = malloc(sizeof(int)*14), *length = malloc(sizeof(int)*14);
	int file_name_buffer_length = strlen(file_name_buffer);
	char* TEMP = malloc(sizeof(char)*file_name_buffer_length);
	
	printf("\n\nDebug : Obtenir le langage (LANGUAGE)\n");
	
	for(i=0; i < 14; i++) islanguage[i]=0;

	char* movie_language[]={ "FRENCH", "ENGLISH", "GERMAN", "SPANISH", "ITALIAN", "RUSSIAN", "CHINESE", "JAPANESE", "ARABEAN" };
	
	char *p = file_name_buffer;
		while (*p != '\0')
		{
			*p = toupper(*p);
			p++;
		}

	for(i = 0; i < 9; i++) {
		char *str = strstr(file_name_buffer, movie_language[i]);
		file_name_buffer_length = strlen(file_name_buffer);
		if(str != NULL) {
			st = 1;
			int str_length = strlen(str);
			n++;
			islanguage[i]=1;
			length[i] = strlen(movie_language[i]);
			printf("l[%d] = %d\n",i,length[i]);

			for(j=0; j<file_name_buffer_length - str_length;j++)
				TEMP[j] = file_name_buffer[j];
			
			printf("temp = %s\n", TEMP);
			
			for(j=file_name_buffer_length - str_length; j<file_name_buffer_length; j++)
				TEMP[j]=file_name_buffer[j+length[i]];
			printf("temp = %s\n", TEMP);
			
			file_name_buffer = TEMP;
		}
		
	}

	
      if(st){
      	LANGUAGE = malloc(sizeof(char)*(8*n+n-1));
	for(i = 0; i < 9; i++) {
		if(islanguage[i]==1) 
			for(j=k; j<k+length[i]; j++) 
				LANGUAGE[j] = movie_language[i][j-k];
	LANGUAGE[j] = ' ';
	k = j+1;
	}
       }
	

}



void getDATE() {

	int file_name_buffer_length = strlen(file_name_buffer);
	int i, position =-1;
	DATE = malloc( sizeof(char)*4);
	
	printf("\n\nDebug : Obtenir la date (DATE)\n");
	
	for(i=0; i<file_name_buffer_length; i++) 		
		switch(file_name_buffer[i]) {
			case 49:
				if(file_name_buffer[i+1] == 57) {
					if(file_name_buffer[i+2] >= 48 && file_name_buffer[i+2] <= 57) {
						if(file_name_buffer[i+3] >= 48 && file_name_buffer[i+3] <= 57) {
							if(file_name_buffer[i+4] >= 48 && file_name_buffer[i+4] <= 57) {
								i=i+4;
							}
							else {
								position = i;
							
							}
								
						}
						else
							i=i+3;
					}
					else
						i=i+2;
				
				
				}
				else
					i++;
				break;
			
			
			case 50:
				if(file_name_buffer[i+1] == 48) {
					if(file_name_buffer[i+2] >= 48 && file_name_buffer[i+2] <= 57) {
						if(file_name_buffer[i+3] >= 48 && file_name_buffer[i+3] <= 57) {
							if(file_name_buffer[i+4] >= 48 && file_name_buffer[i+4] <= 57) {
								i=i+4;
							}
							else {
								position = i;
							
							}
								
						}
						else
							i=i+3;
					}
					else
						i=i+2;
				
				
				
				}
				else
					i++;
				break;
		
		}
		
		
		
	printf("%c%c%c%c : %d / %d\n", file_name_buffer[position], file_name_buffer[position+1], file_name_buffer[position+2], file_name_buffer[position+3], position, file_name_buffer_length);
	
	if(position >= 0) 
		for(i=0; i<4; i++)
			DATE[i]=file_name_buffer[position+i];	
	
	char* TEMP = malloc(sizeof(char)*file_name_buffer_length);
	
	for(i=0; i<position;i++)
		TEMP[i] = file_name_buffer[i];
			
			printf("temp = %s\n", TEMP);
			
	for(i=position; i<file_name_buffer_length; i++)
				TEMP[i]=file_name_buffer[i+4];
			printf("temp = %s\n", TEMP);
	
	file_name_buffer = TEMP;
}

void getTITLE() {
	int i,j =0,k;
	int file_name_buffer_length = strlen(file_name_buffer);
	int *tab = malloc(sizeof(int)*file_name_buffer_length);
	char* TEMP = malloc(sizeof(int)*file_name_buffer_length);
	
	printf("\n\nDebug : Obtenir le titre du film (TITLE)\n");
	
	for(i=0; i<file_name_buffer_length; i++)
		tab[i] = 0;
		
		
	for(i=0; i<file_name_buffer_length; i++) {
		
		switch(file_name_buffer[i]) {
		
			case 40:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					
					if(file_name_buffer[j] == 41) {
						for(k=i;k<=j;k++)
							printf("%c",file_name_buffer[k]);
						printf("\n");
						if(tab[i]==0)
							tab[i] = j;
					
					}				
					
				}
				break;
				
					
			
			case 91:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					if(file_name_buffer[j] == 93) {
						for(k=i;k<=j;k++)
							printf("%c",file_name_buffer[k]);
						printf("\n");
						if(tab[i]==0) 
							tab[i] = j;
					}
					
				}
				break;
			
			
			case 123:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					if(file_name_buffer[j] == 125) {
						for(k=i;k<=j;k++)
							printf("%c",file_name_buffer[k]);
						printf("\n");
						if(tab[i]==0)
							tab[i] = j;
					}
					
				}
				break;
			
			case 60:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					if(file_name_buffer[j] == 62) {
						for(k=i;k<=j;k++)
							printf("%c",file_name_buffer[k]);
						printf("\n");
						if(tab[i]==0)
							tab[i] = j;
					}
					
				}
				break;
		
		
		}
	}
		
	for(i=0; i<file_name_buffer_length; i++)
		if(tab[i] !=0)
			for(j=i;j<=tab[i];j++)
				file_name_buffer[j]=' ';
	
	for(i=0; i<file_name_buffer_length; i++)
		if(	(file_name_buffer[i] > 39 
			&& file_name_buffer[i] < 43)
			
			|| (file_name_buffer[i] == 46)
			|| (file_name_buffer[i] == 47)
			
			|| (file_name_buffer[i] > 58
			&& file_name_buffer[i] < 63)
			
			|| file_name_buffer[i] > 90 )
			
				file_name_buffer[i] = ' ';
	
	i=0;	
	while(file_name_buffer[i++] == ' '); 
		
	for(j=i-1;j<file_name_buffer_length; j++)
		TEMP[j-i+1] = file_name_buffer[j];
	
	char *p = TEMP;
		while (*p != '\0')
		{
			*p = tolower(*p);
			p++;
		}
	char *c = TEMP;
	*c = toupper(*c);
	
	printf("temp = %s\n", TEMP);
	file_name_buffer = TEMP;
	
	
	file_name_buffer_length = strlen(file_name_buffer);
	TITLE = malloc(sizeof(char)*file_name_buffer_length);	
	TITLE = file_name_buffer;
}


int main(int argc, char** argv){

	getEXT();

	printf("\nCe qu'on récupère :\next = %s\n\n", EXT);
	printf("\nCe qui reste du nom de fichier :\nfile_name_buffer = %s\n\n", file_name_buffer);

	getVIDEO();

	printf("\nCe qu'on récupère :\nvideo = %s\n\n", VIDEO);
	printf("\nCe qui reste du nom de fichier :\nfile_name_buffer = %s\n\n", file_name_buffer);
	
	getLANGUAGE();
	
	printf("\nCe qu'on récupère :\nlanguage = %s\n\n", LANGUAGE);
	printf("\nCe qui reste du nom de fichier :\nfile_name_buffer = %s\n\n", file_name_buffer);
	
	getDATE();
	
	printf("\nCe qu'on récupère :\ndate = %s\n\n", DATE);
	printf("\nCe qui reste du nom de fichier :\nfile_name_buffer = %s\n\n", file_name_buffer);
	
	getTITLE();
	
	printf("\nCe qu'on récupère :\ntitle = %s\n\n", TITLE);
	printf("\nCe qui reste du nom de fichier :\nfile_name_buffer = %s\n\n", file_name_buffer);



	printf("\nLes éléments récupérés :\n-->EXT : %s\n-->VIDEO : %s\n-->LANGUAGE : %s\n-->DATE : %s\n-->TITLE : %s\n",EXT,VIDEO,LANGUAGE,DATE,TITLE);

	return 0;
}
