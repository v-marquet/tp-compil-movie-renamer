#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h> // pour char* basename(char* file) et char* dirname(char* file)
#include <ctype.h>

// variables globales
char* path_to_file = NULL;  // chemin du fichier sans son nom
char* file_name = NULL;  // nom du fichier
char* file_name_buffer = NULL;  // buffer pour les modifs, avant le rename

char* EXT=NULL;
char* VIDEO=NULL;
char* LANGUAGE=NULL;
char* DATE=NULL;
char* TITLE=NULL;

char* tab;

// prototypes
void getEXT(); //récupère l'extension du fichier en entrée
void getVIDEO(); //récupère la qualité du film
void getLANGUAGE(); //récupère la langue du film
void getDATE(); //récupère l'année de sortie du film
void getTITLE(); //récupère le titre du film

void apply(); // applique le changement de nom

void set();

//int setSyntax(int num, ...); // initialise le format de la sortie
int setSyntax_(char* string, int defaut); //indique les formats de sorties demande par l'utilisateur

int main(int argc, char* argv[]) {
	int i, n=1;
	if(LANGUAGE != NULL)
		n++;
	if(DATE != NULL)
		n++;
	if(VIDEO != NULL)
		n++;
	// on vérifie qu'on a bien le bon nombre d'arguments
	if (argc != 2) {
		printf("USAGE: ./movie-renamer FILE_TO_RENAME \n");
		return 0;
	}

	// on sépare le path et le nom du fichier
	file_name = basename(argv[1]);
	path_to_file = dirname(argv[1]);

	// on copie file_name dans file_name_buffer
	file_name_buffer = malloc(sizeof(char)*256);
	// (sous Linux, on nom de fichier est limité à 255 caractères)
	strcpy(file_name_buffer, file_name);

	printf("Chemin du fichier: %s \n", argv[1]);
	printf("PATH: %s \n", path_to_file);
	printf("Nom du ficher sans le PATH: %s \n\n", file_name);
	
	getEXT();
	printf("EXT = %s\n",EXT);
	getVIDEO();
	printf("video = %s\n",VIDEO);
	getLANGUAGE();
	printf("language = %s\n",LANGUAGE);
	getDATE();
	printf("date = %s\n",DATE);
	getTITLE();
	printf("title = %s\n\n",TITLE);
	file_name_buffer = NULL;

	
	tab = malloc(sizeof(char)*5);
	//ordre par défaut : titre, date, langage, qualité video, extension
	tab[0] = 't';
	tab[1] = 'd';
	tab[2] = 'l';
	tab[3] = 'v';
	tab[4] = 'e';
	
	file_name_buffer = malloc(sizeof(char)*256);
	strcat(file_name_buffer,TITLE);
	if(DATE != NULL) {
		strcat(file_name_buffer," (");
		strcat(file_name_buffer,DATE);
		strcat(file_name_buffer,") ");
		}
	if(LANGUAGE != NULL) {
		strcat(file_name_buffer,LANGUAGE);
		}
	if(VIDEO != NULL) {
		strcat(file_name_buffer," [");
		strcat(file_name_buffer,VIDEO);
		strcat(file_name_buffer,"]");
	}
	strcat(file_name_buffer,EXT);
	
	printf("En appliquant le Format par défaut, le nouveau nom de votre fichier sera : %s\nQue voulez-vous faire? (help pour afficher la liste des commandes possibles)\n",file_name_buffer);
	
	char* buffer = malloc(sizeof(char)*256); // pour stocker l'instruction tapée par l'utilisateur
	while(scanf("%s", buffer)) {  // Interpréteur de Commande
		
		if(!strcmp(buffer, "help")) {
			printf("\nhelp : Display this Help\n");
			printf("exit : Exit the Program\n");
			printf("apply : Rename the file on disk \n");
			printf("setSyntax : Set an other format to apply\n");
			printf("set : Set another value of the choosen element\n");
		}
		else if(!strcmp(buffer, "exit"))
			return(0);
		else if(!strcmp(buffer, "apply")) {
			apply();
			return 0;	
		}
		
		else if(!strcmp(buffer, "set"))			
			set();	
		
		
		
		else if(!strcmp(buffer, "setSyntax")) {
			printf("Donnez les éléments de nom que vous souhaitez dans l'ordre que vous désirez, séparés par des virgules et terminez par un point. Il est inutile de préciser l'extension.\nExemple : TITLE,DATE,LANGUAGE,VIDEO.\n");
			char *seq = malloc(sizeof(char)*256);
			scanf("%s",seq);
			n=setSyntax_(seq, n);
			}
		else
			printf("Erreur : Commande incorrecte\n");
				
		file_name_buffer = NULL;
		file_name_buffer = malloc(sizeof(char)*256);
		
		for(i=0;i<n;i++) {
			if(tab[i]=='t') {
				strcat(file_name_buffer,TITLE);
				strcat(file_name_buffer," ");
			}
		
			if(tab[i]=='d') {
				strcat(file_name_buffer,"(");
				strcat(file_name_buffer,DATE);
				strcat(file_name_buffer,") ");
			}
			
			if(tab[i]=='l')
				strcat(file_name_buffer,LANGUAGE);
				
			
		
			if(tab[i]=='v'){
				strcat(file_name_buffer,"[");
				strcat(file_name_buffer,VIDEO);
				strcat(file_name_buffer,"]");
			}
			
			
		}
		
		strcat(file_name_buffer,EXT);
		
		printf("\nPour le moment, le nouveau nom de votre fichier est : %s\nQue voulez-vous faire?\n",file_name_buffer);
		
		
		// ici, en fonction de la fonction appelée par l'utilisateur,
		// on appelle une de nos fonctions

	}



	return(0);
}

void set() {

	char* keyword = malloc(sizeof(char)*256);
	char* value = malloc(sizeof(char)*256);
	printf("Quel élément souhaitez-vous modifier ? (TITLE, DATE, VIDEO, ou LANGUAGE)\n");
	scanf("%s", keyword);
	
	if(strcmp(keyword, "TITLE") && strcmp(keyword, "LANGUAGE") && strcmp(keyword, "DATE") && strcmp(keyword, "VIDEO")) {
		printf("Argument incorrect\n");
		return;
		}
	
	printf("Comment voulez-vous le renommer ?\n");
	scanf("%s", value);
			
	if(!strcmp(keyword, "TITLE"))
		TITLE = value;
	else if(!strcmp(keyword, "VIDEO"))
		VIDEO = value;
	else if(!strcmp(keyword, "LANGUAGE"))
		LANGUAGE = value;
	else if(!strcmp(keyword, "DATE"))
		DATE = value;
	

}


// applique le changement de nom au fichier
void apply() {

	char* start_file = malloc(sizeof(char)*256);
	char* finish_file = malloc(sizeof(char)*256);
	
	strcat(start_file, path_to_file);
	strcat(start_file, "/");
	strcat(start_file, file_name);
	printf("start : %s\n",start_file);
	
	strcat(finish_file, path_to_file);
	strcat(finish_file, "/");
	strcat(finish_file, file_name_buffer);
	printf("finish : %s\n",finish_file);
	
	// il faudrait vérifier qu'on ne va pas écraser un autre fichier s'il port le même nom
	rename(start_file, finish_file);
		
	return;
}

int setSyntax_(char* string,int defaut){
	
	int i,j, save=0, n=0;
	int taille=strlen(string);
	for(i=0 ; i<taille ; i++) {
		//traitement de la chaine de caracteres et changement des variables globales
		if(i-save > 8) {
			printf("Argument invalide\n");
			return 4;
			}
			
		if(string[i] == ',' || string[i] == '.') {
			
			char* TEMP = malloc(sizeof(char)*8);
			for(j=save;j<i;j++) 
				TEMP[j-save]=string[j];
			
			if(!strcmp(TEMP, "TITLE"))
				tab[n]='t';
			else if(!strcmp(TEMP, "DATE") && DATE != NULL)
				tab[n]='d';
			else if(!strcmp(TEMP, "VIDEO") && VIDEO != NULL)
				tab[n]='v';
			else if(!strcmp(TEMP, "LANGUAGE") && LANGUAGE != NULL)
				tab[n]='l';
			else{
				printf("Argument incorrecte\n");
				return defaut;
			}
			save=i+1;
			n++;
		}
	}
	return n;
}

void getEXT(){
	int file_name_buffer_length = strlen(file_name_buffer);
	int i=file_name_buffer_length-1,j;
	char* TEMP=NULL;
	EXT = malloc( sizeof(char)*(file_name_buffer_length-i+1) );	
	
	while(file_name_buffer[i]!='.') i--;

	for(j=i; j<file_name_buffer_length; j++) EXT[j-i]=file_name_buffer[j];

	TEMP = malloc( sizeof(char)*(i) );
	for(j=0; j<i;j++) TEMP[j]=file_name_buffer[j];
	
	TEMP[j++] = '\0';

	file_name_buffer = TEMP;
}

void getVIDEO(){
	
	int i, j, n = 0, k =0, st =0;
	int *isquality = malloc(sizeof(int)*14), *length = malloc(sizeof(int)*14);
	int file_name_buffer_length = strlen(file_name_buffer);
	char* TEMP = malloc(sizeof(char)*file_name_buffer_length);
	
	for(i=0; i < 14; i++) isquality[i]=0;

	char* movie_quality[]={ "DVDRIP", "BRRIP", "BLURAY", "BLU-RAY", "480P", "576P", "720P", "1080P", "XVID", "DIVX", "X264", "MPEG2", "MPEG4", "AVC" };

	char *p = file_name_buffer;
		while (*p != '\0')
		{
			*p = toupper(*p);
			p++;
		}

	for(i = 0; i < 14; i++) {
		char *str = strstr(file_name_buffer, movie_quality[i]);
		file_name_buffer_length = strlen(file_name_buffer);
		if(str != NULL) {
			st = 1;
			int str_length = strlen(str);
			n++;
			isquality[i]=1;
			length[i] = strlen(movie_quality[i]);

			for(j=0; j<file_name_buffer_length - str_length;j++)
				TEMP[j] = file_name_buffer[j];
			
			for(j=file_name_buffer_length - str_length; j<file_name_buffer_length; j++)
				TEMP[j]=file_name_buffer[j+length[i]];
		
		
			file_name_buffer = TEMP;
			}
	}

  if(st) {
	VIDEO = malloc(sizeof(char)*(7*n+n-1));
	
	for(i = 0; i < 14; i++) {
		if(isquality[i]==1) 
			for(j=k; j<k+length[i]; j++) 
				VIDEO[j] = movie_quality[i][j-k];
	VIDEO[j] = ' ';
	k = j+1;
	}
   }
	
}

void getLANGUAGE() {

	int i, j, n = 0, k =0, st = 0;
	int *islanguage = malloc(sizeof(int)*14), *length = malloc(sizeof(int)*14);
	int file_name_buffer_length = strlen(file_name_buffer);
	char* TEMP = malloc(sizeof(char)*file_name_buffer_length);
	
	for(i=0; i < 14; i++) islanguage[i]=0;

	char* movie_language[]={ "FRENCH", "ENGLISH", "GERMAN", "SPANISH", "ITALIAN", "RUSSIAN", "CHINESE", "JAPANESE", "ARABEAN" };
	
	char *p = file_name_buffer;
		while (*p != '\0')
		{
			*p = toupper(*p);
			p++;
		}

	for(i = 0; i < 9; i++) {
		char *str = strstr(file_name_buffer, movie_language[i]);
		file_name_buffer_length = strlen(file_name_buffer);
		if(str != NULL) {
			st = 1;
			int str_length = strlen(str);
			n++;
			islanguage[i]=1;
			length[i] = strlen(movie_language[i]);

			for(j=0; j<file_name_buffer_length - str_length;j++)
				TEMP[j] = file_name_buffer[j];
			
			for(j=file_name_buffer_length - str_length; j<file_name_buffer_length; j++)
				TEMP[j]=file_name_buffer[j+length[i]];
			
			file_name_buffer = TEMP;
		}
		
	}

	
      if(st){
      	LANGUAGE = malloc(sizeof(char)*(8*n+n-1));
	for(i = 0; i < 9; i++) {
		if(islanguage[i]==1) 
			for(j=k; j<k+length[i]; j++) 
				LANGUAGE[j] = movie_language[i][j-k];
	LANGUAGE[j] = ' ';
	k = j+1;
	}
       }
	

}



void getDATE() {

	int file_name_buffer_length = strlen(file_name_buffer);
	int i, position =-1;
	DATE = malloc( sizeof(char)*4);
	
	for(i=0; i<file_name_buffer_length; i++) 		
		switch(file_name_buffer[i]) {
			case 49:
				if(file_name_buffer[i+1] == 57) {
					if(file_name_buffer[i+2] >= 48 && file_name_buffer[i+2] <= 57) {
						if(file_name_buffer[i+3] >= 48 && file_name_buffer[i+3] <= 57) {
							if(file_name_buffer[i+4] >= 48 && file_name_buffer[i+4] <= 57) {
								i=i+4;
							}
							else {
								position = i;
							
							}
								
						}
						else
							i=i+3;
					}
					else
						i=i+2;
				
				
				}
				else
					i++;
				break;
			
			
			case 50:
				if(file_name_buffer[i+1] == 48) {
					if(file_name_buffer[i+2] >= 48 && file_name_buffer[i+2] <= 57) {
						if(file_name_buffer[i+3] >= 48 && file_name_buffer[i+3] <= 57) {
							if(file_name_buffer[i+4] >= 48 && file_name_buffer[i+4] <= 57) {
								i=i+4;
							}
							else {
								position = i;
							
							}
								
						}
						else
							i=i+3;
					}
					else
						i=i+2;
				
				
				
				}
				else
					i++;
				break;
			
			default:
				break;
		
		}
		
		
	if(position >= 0) {
	for(i=0; i<4; i++)
			DATE[i]=file_name_buffer[position+i];
		
	
	char* TEMP = malloc(sizeof(char)*file_name_buffer_length);
	
	for(i=0; i<position;i++)
		TEMP[i] = file_name_buffer[i];
			
	for(i=position; i<file_name_buffer_length; i++)
				TEMP[i]=file_name_buffer[i+4];
	
	file_name_buffer = TEMP;
	}
	else
		DATE = NULL;
}

void getTITLE() {
	int i,j =0,k;
	int file_name_buffer_length = strlen(file_name_buffer);
	int *tab = malloc(sizeof(int)*file_name_buffer_length);
	char* TEMP = malloc(sizeof(int)*file_name_buffer_length);
	
	for(i=0; i<file_name_buffer_length; i++)
		tab[i] = 0;
		
		
	for(i=0; i<file_name_buffer_length; i++) {
		
		switch(file_name_buffer[i]) {
		
			case 40:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					
					if(file_name_buffer[j] == 41) {
						if(tab[i]==0)
							tab[i] = j;
					
					}				
					
				}
				break;
				
					
			
			case 91:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					if(file_name_buffer[j] == 93) {
						if(tab[i]==0) 
							tab[i] = j;
					}
					
				}
				break;
			
			
			case 123:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					if(file_name_buffer[j] == 125) {
						for(k=i;k<=j;k++)
						if(tab[i]==0)
							tab[i] = j;
					}
					
				}
				break;
			
			case 60:
				
				for(j=i+1; j<file_name_buffer_length; j++) {
					if(file_name_buffer[j] == 62) {
						if(tab[i]==0)
							tab[i] = j;
					}
					
				}
				break;
		
		
		}
	}
		
	for(i=0; i<file_name_buffer_length; i++)
		if(tab[i] !=0)
			for(j=i;j<=tab[i];j++)
				file_name_buffer[j]=' ';
	
	for(i=0; i<file_name_buffer_length; i++)
		if(	(file_name_buffer[i] > 39 
			&& file_name_buffer[i] < 43)
			
			|| (file_name_buffer[i] == 46)
			|| (file_name_buffer[i] == 47)
			
			|| (file_name_buffer[i] > 58
			&& file_name_buffer[i] < 63)
			
			|| file_name_buffer[i] > 90 )
			
				file_name_buffer[i] = ' ';
	
	i=0;	
	while(file_name_buffer[i++] == ' '); 
		
	for(j=i-1;j<file_name_buffer_length; j++)
		TEMP[j-i+1] = file_name_buffer[j];
	
	char *p = TEMP;
		while (*p != '\0')
		{
			*p = tolower(*p);
			p++;
		}
	char *c = TEMP;
	*c = toupper(*c);
	
	file_name_buffer =TEMP;
	
	i=strlen(file_name_buffer);	
	while(file_name_buffer[i--] == ' ');
	for(j=0;j<i;j++)
		TEMP[i] = file_name_buffer[i];
	file_name_buffer = TEMP;
	
	
	file_name_buffer_length = strlen(file_name_buffer);
	TITLE = malloc(sizeof(char)*file_name_buffer_length);	
	TITLE = file_name_buffer;
}

