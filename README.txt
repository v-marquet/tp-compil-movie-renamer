Pour ce projet, nous avons eu l'idée de faire un programme qui permette, à travers une invite de commande, d'offrir des fonctions qui permettent de renommer des fichiers vidéos afin qu'ils aient un nom propre. En effet, sur internet, il n'y a pas de convention officielle pour nommer les films disponibles que les gens s'échangent via les réseaux P2P. On trouve par exemple:

[kickass.to](2000.laws).pirates.des.caraibes.1.la.malediction.du.black.pearl.1980.french.{2900189880234564}.dvdrip.divx.avi

Or, de nombreux players vidéos, comme XBMC, permettent, en plus de lire les fichiers vidéos, de récupérer des informations sur des bases de données dédiées au cinéma comme le site RottenTomatoes. Cependant, ces logiciels se basent sur le nom du fichier pour faire leur recherche. Or, un nom du fichier comme celui donné précédemment ne permet pas de trouver les résultats escomptés dans les bases de données. Notre programme permet donc de renommer ces fichiers afin d'une part, que le nom soit plus agréable à l'oeil, et d'autre part, que les recherches sur des bases de données trouvent les bons résultats.

Etat d'avancement:
- le programme fonctionne globalement bien, même si parfois, il y a des espaces en trop qui apparraissent dans le nouveau nom à donner au fichier. Il sont simplement dûs à des erreurs en manipulant des char*, mais nous n'avons pas le temps de les corriger.
- nous aurions aimé utiliser Lex et Yacc pour faire ce programme, mais pour cause de manque de temps (plusieurs partiels...), nous n'avons pas pu.

Voici la ligne de compilation :
gcc -Wall main.c -o main

Ensuite, pour lancer le programme, utilisez la ligne suivante :
./main fichier_à_renommer

